# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qtcolorfilter.ui'
#
# Created: Tue Mar 29 01:26:18 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ColorFilter(object):
    def setupUi(self, ColorFilter):
        ColorFilter.setObjectName(_fromUtf8("ColorFilter"))
        ColorFilter.resize(676, 497)
        self.gridLayout_2 = QtGui.QGridLayout(ColorFilter)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout_root = QtGui.QGridLayout()
        self.gridLayout_root.setMargin(9)
        self.gridLayout_root.setObjectName(_fromUtf8("gridLayout_root"))
        self.horizontalLayout_top = QtGui.QHBoxLayout()
        self.horizontalLayout_top.setSpacing(10)
        self.horizontalLayout_top.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.horizontalLayout_top.setObjectName(_fromUtf8("horizontalLayout_top"))
        self.uiLabel_left = QtGui.QLabel(ColorFilter)
        self.uiLabel_left.setObjectName(_fromUtf8("uiLabel_left"))
        self.horizontalLayout_top.addWidget(self.uiLabel_left)
        self.uiLabel_right = QtGui.QLabel(ColorFilter)
        self.uiLabel_right.setObjectName(_fromUtf8("uiLabel_right"))
        self.horizontalLayout_top.addWidget(self.uiLabel_right)
        self.gridLayout_root.addLayout(self.horizontalLayout_top, 0, 0, 3, 3)
        self.verticalLayout_aux = QtGui.QVBoxLayout()
        self.verticalLayout_aux.setSpacing(10)
        self.verticalLayout_aux.setObjectName(_fromUtf8("verticalLayout_aux"))
        self.uiPushButton_fit = QtGui.QPushButton(ColorFilter)
        self.uiPushButton_fit.setObjectName(_fromUtf8("uiPushButton_fit"))
        self.verticalLayout_aux.addWidget(self.uiPushButton_fit)
        self.uiPushButton_reset = QtGui.QPushButton(ColorFilter)
        self.uiPushButton_reset.setObjectName(_fromUtf8("uiPushButton_reset"))
        self.verticalLayout_aux.addWidget(self.uiPushButton_reset)
        self.gridLayout_root.addLayout(self.verticalLayout_aux, 3, 2, 1, 1)
        self.horizontalLayout_bottom = QtGui.QHBoxLayout()
        self.horizontalLayout_bottom.setSpacing(6)
        self.horizontalLayout_bottom.setObjectName(_fromUtf8("horizontalLayout_bottom"))
        self.uiLabel_1 = QtGui.QLabel(ColorFilter)
        self.uiLabel_1.setObjectName(_fromUtf8("uiLabel_1"))
        self.horizontalLayout_bottom.addWidget(self.uiLabel_1)
        self.verticalLayout_1 = QtGui.QVBoxLayout()
        self.verticalLayout_1.setObjectName(_fromUtf8("verticalLayout_1"))
        self.uiHorizontalSlider_1a = QtGui.QSlider(ColorFilter)
        self.uiHorizontalSlider_1a.setMaximum(180)
        self.uiHorizontalSlider_1a.setPageStep(10)
        self.uiHorizontalSlider_1a.setOrientation(QtCore.Qt.Horizontal)
        self.uiHorizontalSlider_1a.setObjectName(_fromUtf8("uiHorizontalSlider_1a"))
        self.verticalLayout_1.addWidget(self.uiHorizontalSlider_1a)
        self.uiHorizontalSlider_1b = QtGui.QSlider(ColorFilter)
        self.uiHorizontalSlider_1b.setMaximum(180)
        self.uiHorizontalSlider_1b.setProperty("value", 180)
        self.uiHorizontalSlider_1b.setOrientation(QtCore.Qt.Horizontal)
        self.uiHorizontalSlider_1b.setObjectName(_fromUtf8("uiHorizontalSlider_1b"))
        self.verticalLayout_1.addWidget(self.uiHorizontalSlider_1b)
        self.horizontalLayout_bottom.addLayout(self.verticalLayout_1)
        self.uiLabel_2 = QtGui.QLabel(ColorFilter)
        self.uiLabel_2.setObjectName(_fromUtf8("uiLabel_2"))
        self.horizontalLayout_bottom.addWidget(self.uiLabel_2)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.uiHorizontalSlider_2a = QtGui.QSlider(ColorFilter)
        self.uiHorizontalSlider_2a.setMaximum(255)
        self.uiHorizontalSlider_2a.setOrientation(QtCore.Qt.Horizontal)
        self.uiHorizontalSlider_2a.setObjectName(_fromUtf8("uiHorizontalSlider_2a"))
        self.verticalLayout_2.addWidget(self.uiHorizontalSlider_2a)
        self.uiHorizontalSlider_2b = QtGui.QSlider(ColorFilter)
        self.uiHorizontalSlider_2b.setMaximum(255)
        self.uiHorizontalSlider_2b.setProperty("value", 255)
        self.uiHorizontalSlider_2b.setOrientation(QtCore.Qt.Horizontal)
        self.uiHorizontalSlider_2b.setObjectName(_fromUtf8("uiHorizontalSlider_2b"))
        self.verticalLayout_2.addWidget(self.uiHorizontalSlider_2b)
        self.horizontalLayout_bottom.addLayout(self.verticalLayout_2)
        self.uiLabel_3 = QtGui.QLabel(ColorFilter)
        self.uiLabel_3.setObjectName(_fromUtf8("uiLabel_3"))
        self.horizontalLayout_bottom.addWidget(self.uiLabel_3)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.uiHorizontalSlider_3a = QtGui.QSlider(ColorFilter)
        self.uiHorizontalSlider_3a.setMaximum(255)
        self.uiHorizontalSlider_3a.setOrientation(QtCore.Qt.Horizontal)
        self.uiHorizontalSlider_3a.setObjectName(_fromUtf8("uiHorizontalSlider_3a"))
        self.verticalLayout_3.addWidget(self.uiHorizontalSlider_3a)
        self.uiHorizontalSlider_3b = QtGui.QSlider(ColorFilter)
        self.uiHorizontalSlider_3b.setMaximum(255)
        self.uiHorizontalSlider_3b.setProperty("value", 255)
        self.uiHorizontalSlider_3b.setOrientation(QtCore.Qt.Horizontal)
        self.uiHorizontalSlider_3b.setObjectName(_fromUtf8("uiHorizontalSlider_3b"))
        self.verticalLayout_3.addWidget(self.uiHorizontalSlider_3b)
        self.horizontalLayout_bottom.addLayout(self.verticalLayout_3)
        self.gridLayout_root.addLayout(self.horizontalLayout_bottom, 3, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout_root, 0, 0, 1, 1)

        self.retranslateUi(ColorFilter)
        QtCore.QMetaObject.connectSlotsByName(ColorFilter)

    def retranslateUi(self, ColorFilter):
        ColorFilter.setWindowTitle(_translate("ColorFilter", "ColorFilter", None))
        self.uiLabel_left.setText(_translate("ColorFilter", "LabelLeft", None))
        self.uiLabel_right.setText(_translate("ColorFilter", "LabelRight", None))
        self.uiPushButton_fit.setText(_translate("ColorFilter", "1:1", None))
        self.uiPushButton_reset.setText(_translate("ColorFilter", "Reset", None))
        self.uiLabel_1.setText(_translate("ColorFilter", "H", None))
        self.uiLabel_2.setText(_translate("ColorFilter", "S", None))
        self.uiLabel_3.setText(_translate("ColorFilter", "V", None))

