import sys
from PyQt4 import QtGui
from qtcolorfilter_ui import Ui_ColorFilter

# generate ui
# pyuic4 qtcolorfilter.ui > qtcolorfilter_ui.py


class ColorFilterWidget(QtGui.QWidget):
  def __init__(self, parent=None, flags=0):
    QtGui.QWidget.__init__(self, parent)
    self.ui = Ui_ColorFilter()
    self.ui.setupUi(self)





app = QtGui.QApplication(sys.argv)
widget = ColorFilterWidget()
widget.show()

app.exec_()
